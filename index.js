    const express = require('express')
    const path = require('path');
    const logger = require('morgan')
    const cookieParser = require('cookie-parser');
    const bodyParser = require('body-parser');
    const session = require('express-session');

    var loginManager = require('./routes/loginManager');
    var posts        = require('./routes/posts');
    var friends      = require('./routes/friendManager');
    var router = express.Router();

    const app     = express() 
    const port    = 5000

    // handle session
    app.use(session({
        secret : 'tUpPe6wa6e',
        resave: true,
        saveUninitialized: false
    }));

    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'pug');

    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));

    app.use(logger('dev', {
        skip:  (req, res) => {return res.statusCode >= 400},
        stream: process.stdout
    }));

    app.use(bodyParser.json()); 
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use('/api',router);

    app.get('/',function(req,res){
        if(req.session.user_id) res.render('home',{user:req.session.user})
        else res.redirect('/login');
    });

    app.get('/login',(req,res)=>{res.render('login'); })
    app.get('/signin',(req,res)=>{res.render('signup')})
    app.get('/profile',(req,res)=>{res.render('profile')})
    app.get('/requests',(req,res)=>{res.redirect('/api/pending')})

    router.post('/register',loginManager.register);
    router.post('/login',loginManager.login);
    router.get('/logout',loginManager.logout);

    router.post('/post',posts.insertPost);

    router.get('/searchFriends',friends.search);
    router.get('/sendRequest',friends.request);
    router.get('/pending',friends.pending);
    router.get('/acceptRequest',friends.acceptRequest);

    router.get('/myfriends',friends.myfriends);

    app.get('/post',posts.getPosts);

    app.get('/username',loginManager.getName);

    app.listen(port,'0.0.0.0',function(){
        console.log(`running on the server ${port}`)
    })