const mysql = require('mysql');

var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "textbook",
    insecureAuth : true
});
  
connection.connect();

exports.insertPost = function(req,res){

    const user_id = req.session.user_id;
    const post    = req.body.post;

    connection.query(`insert into posts (user_id,post,time) values ('${user_id}','${post}',now())`,function(error,result,fields){

        if(error) res.status(400).send(error);
        else res.redirect("/")

     });

}

exports.getPosts = function(req,res){

    var query=`select username,post from posts p,users u where p.user_id in (select friend_id from friends f where f.user_id = '${req.session.user_id}') and p.user_id=u.user_id`;
    connection.query(query,function(error,result,fields){

        if(error) res.send(error);
        else{
        
            var postObj = {};
            postObj.data = Array();
            
            result.forEach(element => {
                postObj.data.push(element);
            });

            res.json(postObj);
        }

    });

}