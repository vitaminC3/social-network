const mysql = require('mysql');

var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "textbook",
    insecureAuth : true
});
  
connection.connect();

exports.register = function(req,res){

    const username  = req.body.username;
    const password  = req.body.password;
    const email     = req.body.email;

    // do verification
    connection.query(`insert into users (username,password,email) values ('${username}','${password}','${email}')`,function(error,result,fields){
        if(error) 
            res.status(400).send("failed to create user");
        else{
            connection.query(`select * from users where email = '${email}'`,function(err,result,fld){
                req.session.username = result[0]['username']
                req.session.user_id  = result[0]['user_id']
                res.redirect('/')
            });
        }
    });
}

exports.login = function(req,res){

    const email     = req.body.email;
    const password  = req.body.password;

    connection.query(`select * from users where email = '${email}'`,function(error,result,fields){

        if(error) res.status(400).send("failed to login");
        else{

            if(result.length > 0){
                if(result[0].password == password){

                    connection.query(`select * from users where email = '${email}'`,function(err,result,fld){
                        if(err) res.send("error while loggin in");
                        else{
                            req.session.username = result[0]['username']
                            req.session.user_id  = result[0]['user_id']
                            res.redirect('/')
                        } 
                    });
                }
                else res.status('400').json({"data":"email and password doesn't match"});
            }

        }

    });
}

exports.logout = function(req,res){

    if(req.session.user_id){
        req.session.destroy(function(err){
            if(err) res.send(err);
            else res.redirect("/");
        });
    }
}

exports.getName = function(req,res){
    const user_id = req.query.user_id;
    connection.query(`select username from users where user_id=${user_id}`,function(err,result,flds){
        res.json({"username":result[0].username});
    });

}