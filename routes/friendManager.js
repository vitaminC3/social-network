const mysql = require('mysql');

var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "textbook",
    insecureAuth : true
});
  
connection.connect();

exports.search = function(req,res){
    if(req.session.user_id){

        var query_str = `select user_id,username from users where user_id != ${req.session.user_id} and username like '%${req.query.search}%'`
        connection.query(query_str,function(error,result,fields){
            if(error) res.status(400).send(error)
            else{

                var friends = {};
                friends.data = Array();            

                result.forEach(element => {
                    friends.data.push({id:element['user_id'],username:element['username']})
                });
                
                res.render('friends',{ result:friends.data} );
            }
        });
    }
    else
        res.redirect("/");
    
}

exports.request = function(req,res){
    if(req.session.user_id){

        var query = `insert into friends values ('${req.session.user_id}','${req.query.id}','pending')`;
        
        connection.query(query,function(error,result,fields){

            if(error) res.status(400).send(error)
            else res.redirect('/');
        });

    }
    else
        res.redirect("/");
}

exports.pending = function(req,res){
    
    if(req.session.user_id){

        var query = `select u.username,u.user_id from users u,friends f where f.friend_id='${req.session.user_id}' and f.user_id = u.user_id and f.status = 'pending'`;
        connection.query(query,function(err,result,fields){

            if(err) res.send(err);
            else{

                var requestsFrom = [];

                result.forEach(element=>{
                    requestsFrom.push({'username':element['username'],'id':element['user_id']});
                });
                
                res.render('friendRequests',{requestsFrom:requestsFrom})
            }

        });
    }
    else{
        res.redirect("/");
    }

}

exports.acceptRequest = function(req,res){

    if(req.session.user_id){

        var query = `update friends set status='friends' where friend_id='${req.session.user_id}' and user_id='${req.query.id}'`
        connection.query(query,function(err,result,fields){

            if(err) res.send(err);
            else{

                var query = `insert into friends values ('${req.session.user_id}','${req.query.id}','friends')`;
                connection.query(query,function(err,result,fields){
                    
                    if(err) res.send(err);
                    else{
                        res.redirect('/');
                    }

                });
            }
        });

    }
    else
        res.redirect("/");

}

exports.myfriends = function(req,res){

    if(req.session.user_id){

        var query = `select u.username,u.user_id from users u,friends f where f.user_id='${req.session.user_id}' and f.friend_id=u.user_id and status='friends'`;
        connection.query(query,function(err,results,fields){

            if(err) res.send(err);
            else{
                
                var myfreinds = [];

                results.forEach(element=>{
                    myfreinds.push({'username':element['username'],'id':element['user_id']});
                });

                res.json(myfreinds);
            }

        });

    }
    else{
        res.redirect("/")
    }

}