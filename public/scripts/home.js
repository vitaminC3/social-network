$(document).ready(function(){
    $.get("/post",function(data){

        // console.log(data.data)
        var len = data.data.length;

        console.log(data);

        if(len == 0){
            var element = document.createElement("div");
            element.id = "empty-news-card";
            element.innerText = "EMPTY";
            document.getElementById("newsfeed").appendChild(element);
        }
        else{
            for(var i=0;i<len;i++){
                var text = `<div class="newscard" style="margin-left:10px">
                                <div class="containerid">
                                    <h5> ${data.data[i]["username"]} </h5>
                                    <hr/>
                                    <p>  ${data.data[i]["post"]} </p>
                                </div>
                            </div> <br/>`
                var element = document.createElement("div");
                element.innerHTML = text;
                document.getElementById("newsfeed").appendChild(element);
            }
        }
    });
    
    
    $.get('/api/myfriends',function(data){

        var len = data.length;

        if(len == 0){
            var element = document.createElement("div");
            element.id = "empty-friends-list";
            element.innerText = "EMPTY";
            document.getElementById("friendsList").appendChild(element);
        }
        else{

            for(var i=0;i<len;i++){

                var text = `<div class="friend">
                                <div class="friend-container">
                                <h5> ${data[i]['username']} </h5>
                            </div>
                `

                var element = document.createElement("div");
                element.innerHTML = text;
                document.getElementById("friendsList").appendChild(element);

            }

        }

    });


});
